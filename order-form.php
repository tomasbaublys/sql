<?php 

    $database = new PDO('mysql:host=localhost;dbname=classicmodels;charset=utf8', 'root', 'root');

    $query = $database->prepare(
        'SELECT 
        customerName, 
            contactFirstName,
            contactLastName, 
            addressLine1,
            addressLine2, 
            city
        FROM customers
        INNER JOIN orders ON orders.customerNumber = customers.customerNumber
        WHERE orderNumber = ?');

    $query->execute(array($_GET['orderNumber']));

    $orders = $query->fetch(PDO::FETCH_ASSOC);

    // Second query

    $query = $database->prepare(
        'SELECT 
        productName, 
        `priceEach`, 
        `quantityOrdered`, 
        orderLineNumber,
        priceEach * quantityOrdered AS totalPrice
        FROM `orderdetails` 
        INNER JOIN products ON products.productCode = orderdetails.productCode
        WHERE orderNumber = 10100
        ORDER BY orderLineNumber');

    $query->execute(array($_GET['orderNumber']));

    $products = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <body>
      <div class="container">
        <h1>Order form</h1>
        <p>Return back</p>
        <h2 class="text-right"><?= $orders['customerName']; ?></h2>
        <h3 class="text-right"><?= $orders['contactFirstName']; ?><?= $orders['contactLastName']; ?></h3>
        <p class="text-right"><?= $orders['addressLine1']; ?><?= $orders['addressLine2']; ?></p>
        <p class="text-right"><?= $orders['city']; ?></p>
        <hr>
        <h3 class="text-center">Order no #<?= $_GET['orderNumber']; ?> </h3>         
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="col-md-5">Product</th>
              <th class="col-md-2">Price</th>
              <th class="col-md-3">Quantity</th>
              <th class="col-md-1">Total</th>
            </tr>
          </thead>
          <tbody>
          <?php $totalPrice = 0; ?>
          <?php foreach($products as $product) : ?>
          <?php

              $rowPrice = round ($product['totalPrice'], 2);
              $totalPrice = $totalPrice + $rowPrice;
              $total = round ($totalPrice * 1.21, 2);
              $vat = round ($total - $totalPrice, 2);

          ?>
            <tr>
              <td><?= $product['productName']; ?></td>
              <td><?= $product['priceEach']; ?></td>
              <td><?= $product['quantityOrdered']; ?></td>
              <td><?= $rowPrice ?></td>
            </tr>
          <?php endforeach; ?>
            <tr>
              <th class="col-md-5"></th>
              <th class="col-md-2"></th>
              <th class="col-md-3">Sub-Total</th>
              <th class="col-md-1"><?= $totalPrice ?></th>
            </tr>
            <tr>
              <th class="col-md-5"></th>
              <th class="col-md-2"></th>
              <th class="col-md-3">VAT(21%)</th>
              <th class="col-md-1"><?= $vat ?></th>
            </tr>
            <tr>
              <th class="col-md-5"></th>
              <th class="col-md-2"></th>
              <th class="col-md-3">Total</th>
              <th class="col-md-1"><?= $total ?></th>
            </tr>
          </tbody>
        </table>
      </div>
  </body>
</html>
