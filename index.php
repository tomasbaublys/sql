<?php 

    $database = new PDO('mysql:host=localhost;dbname=classicmodels;charset=utf8', 'root', 'root');

    $query = $database->prepare(
        'SELECT 
            orderNumber, 
            orderDate, 
            shippedDate, 
            status
        FROM orders
        ORDER BY orderNumber
        LIMIT 20');

    $query->execute();

    $orders = $query->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Fonts -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <body>
      <div class="container">
        <h1>Purchase orders</h1>
        <h2 class="text-center">Purchase list</h2>         
        <table class="table table-striped">
          <thead>
            <tr>
                <th>Purchase</th>
                <th>Date of purchase</th>
                <th>Date of delivery</th>
                <th>Status</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach($orders as $order) : ?>
            <tr>
                <td>
                    <a href="order-form.php?orderNumber=<?= $order['orderNumber']; ?>"?><?= $order['orderNumber']; ?></a>
                </td>
                <td><?= $order['orderDate']; ?></td>
                <td><?= $order['shippedDate']; ?></td>
                <td><?= $order['status']; ?></td>
            </tr>
            <?php endforeach; ?>

          </tbody>
        </table>
      </div>
  </body>
</html>

